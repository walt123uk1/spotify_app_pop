# spotify_app

## Project setup
```
npm install
Need to add valid OAuth_Token to App.vue to get allow search (runs out after 1hr)
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
